from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains

class BasePage:

    def __init__(self, driver):
        self.driver = driver
        self.base_url = "https://demoqa.com/"

    def find_element(self, locator,time=15):
        return WebDriverWait(self.driver,time).until(EC.presence_of_element_located(locator),
                                                      message=f"Не найден {locator}")
        
    def show_element(self, locator):
        return 
        target = browser.find_element(By.XPATH,"locator") 
        actions = ActionChains(browser)
        actions.move_to_element(target)
        actions.perform()

    def go_to_site(self):
        return self.driver.get(self.base_url)